from collaboration_hypergraph.src.utils.model_args import ModelArgs
from collaboration_hypergraph.src.data_processing.ca_hepth.ca_hepth_dataset \
    import CAHepThLinkDataset
from collaboration_hypergraph.src.gnn_models.hypergraphconv_model import \
    HypergraphModel


def test_hypergraphconv_model():
    dataset = CAHepThLinkDataset()
    args = ModelArgs()
    model = HypergraphModel(
        input_dim=dataset.data.x.shape[1],
        hidden_dim=10,
        output_dim=None,
        args=args
    )
    out = model.forward(
        data=dataset.data,
        edge_index=dataset.data.train_pos_edge_index
    )
    assert out.shape == dataset.data.x.shape


if __name__ == '__main__':
    test_hypergraphconv_model()
