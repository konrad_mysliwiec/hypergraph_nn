from neptune.new.run import Run
import os.path as osp
from tensorboardX import SummaryWriter
import torch
import torch.nn as nn
import torch_geometric.nn as pyg_nn
from torch_geometric.loader import DataLoader
import tqdm
from sklearn.metrics import average_precision_score, \
    roc_auc_score
from collaboration_hypergraph.src.utils.utils import create_directory
from collaboration_hypergraph.src.utils.model_args import ModelArgs
from collaboration_hypergraph.src.training.optimizer_util import \
    build_optimizer
from collaboration_hypergraph.src.data_processing.ca_hepth.ca_hepth_dataset \
    import CAHepThLinkDataset


def train(
    model: pyg_nn.GAE,
    dataset: CAHepThLinkDataset,
    args: ModelArgs,
    model_type: str,
    learning_type: str = 'subgraphs',
    writer: SummaryWriter = None,
    neptune_run: Run = None,
    checkpoint_dir: str = None
):
    test_loader = train_loader = DataLoader(
        dataset,
        batch_size=args.batch_size,
        shuffle=False
    )

    _, opt = build_optimizer(args, model.parameters())

    for epoch in tqdm.tqdm(range(args.epochs)):
        total_loss = 0
        model.train()
        train_roc_auc = []
        train_precisions = []
        for batch in train_loader:
            opt.zero_grad()
            hyperedge_index_train = batch.hyperedge_index_train
            train_pos_edge_index = batch.train_pos_edge_index
            train_neg_edge_index = batch.train_neg_edge_index
            if model_type == 'hypergraph':
                z = model.encode(batch, hyperedge_index_train)
            else:
                z = model.encode(batch, train_pos_edge_index)
            loss = model.recon_loss(
                z,
                train_pos_edge_index,
                train_neg_edge_index
            )
            roc_auc, train_precision = model.test(
                z,
                train_pos_edge_index,
                train_neg_edge_index
            )
            train_precisions.append(train_precision)
            train_roc_auc.append(roc_auc)
            loss.backward()
            opt.step()
            total_loss += loss.item() * batch.num_graphs
        total_loss /= len(train_loader.dataset)
        if writer:
            writer.add_scalar("Train Loss", total_loss, epoch)
            writer.add_scalar(
                "Train Average precision",
                train_precision,
                epoch
            )
            writer.add_scalar("Train ROC AUC score", roc_auc, epoch)
        if neptune_run:
            neptune_run['train/loss'].log(total_loss, epoch)
            neptune_run['train/avg_precision'].log(train_precision, epoch)
            neptune_run['train/roc_auc'].log(roc_auc, epoch)

        if epoch % 10 == 0:
            test_checkpoint(
                test_loader=test_loader,
                model=model,
                opt=opt,
                epoch=epoch,
                checkpoint_dir=checkpoint_dir,
                model_type=model_type,
                learning_type=learning_type,
                writer=writer,
                neptune_run=neptune_run
            )


def test_checkpoint(
    test_loader: DataLoader,
    model: nn.Module,
    opt: torch.optim.Optimizer,
    epoch: int,
    checkpoint_dir: str,
    model_type: str,
    learning_type: str = 'subgraphs',
    writer: SummaryWriter = None,
    neptune_run: Run = None
):
    test_total_loss, avg_precision_metric, roc_auc_metric = test(
        test_loader,
        model,
        model_type=model_type,
        learning_type=learning_type
    )
    if writer:
        writer.add_scalar(
            "Test Total Loss",
            test_total_loss,
            epoch
        )
        writer.add_scalar(
            "Test Average precision",
            avg_precision_metric,
            epoch
        )
        writer.add_scalar(
            "Test ROC AUC score",
            roc_auc_metric,
            epoch
        )

    if neptune_run:
        neptune_run['test/total_loss'].log(test_total_loss, epoch)
        neptune_run['test/avg_precision'].log(avg_precision_metric, epoch)
        neptune_run['test/roc_auc'].log(roc_auc_metric, epoch)

    if checkpoint_dir:
        create_directory(checkpoint_dir)
        checkpoint = {
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': opt.state_dict(),
            'epoch': epoch
        }
        checkpoint_file_path = osp.join(
            checkpoint_dir, f'checkpoint_{epoch}.pth'
        )
        torch.save(
            checkpoint,
            checkpoint_file_path
        )


def test(
    loader: DataLoader,
    model: nn.Module,
    model_type: str,
    learning_type: str = 'subgraphs',
    is_validation: bool = False
):
    model.eval()
    test_total_loss = 0
    test_prec = []
    y = []
    pred = []
    pos_preds = []
    neg_preds = []
    for data in loader:
        with torch.no_grad():
            if learning_type == 'subgraphs':
                pos_edge_index = data.train_pos_edge_index
                neg_edge_index = data.train_neg_edge_index
                pos_hyperedge_index = data.hyperedge_index_train
            elif learning_type == 'transductive':
                pos_edge_index = torch.cat(
                    [
                        data.train_pos_edge_index,
                        data.test_pos_edge_index
                    ]
                )
                pos_hyperedge_index = torch.cat(
                    [
                        data.hyperedge_index_train,
                        data.hyperedge_index_test
                    ]
                )
            if model_type == 'hypegraph':
                z = model.encode(data, pos_hyperedge_index)
            else:
                z = model.encode(data, pos_edge_index)
            test_prec.append(
                model.test(
                    z,
                    data.test_pos_edge_index,
                    data.test_neg_edge_index
                )[1].item()
            )
            pos_y = z.new_ones(data.test_pos_edge_index.size(1))
            neg_y = z.new_zeros(data.test_neg_edge_index.size(1))
            y.append(pos_y)
            y.append(neg_y)
            pos_pred = model.decoder(
                z,
                data.test_pos_edge_index,
                sigmoid=True
            )
            neg_pred = model.decoder(
                z,
                data.test_neg_edge_index,
                sigmoid=True
            )
            pred.append(pos_pred)
            pred.append(neg_pred)
            pos_preds.append(pos_pred)
            neg_preds.append(neg_pred)
            test_loss = model.recon_loss(
                z,
                pos_edge_index,
                neg_edge_index
            )
            test_total_loss += test_loss.item() * data.num_graphs
    test_total_loss /= len(loader.dataset)
    y = torch.cat(y, dim=0)
    pred = torch.cat(pred, dim=0)
    return (
        test_total_loss,
        average_precision_score(y, pred),
        roc_auc_score(y, pred)
    )
