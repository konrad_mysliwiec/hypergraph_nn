import click
from datetime import datetime
import os.path as osp
import torch_geometric.nn as pyg_nn
from tensorboardX import SummaryWriter

from collaboration_hypergraph.src.data_processing.ca_hepth.ca_hepth_dataset \
    import CAHepThLinkDataset
from collaboration_hypergraph.src.utils import settings
from collaboration_hypergraph.src.training.ca_hepth.train import train
from collaboration_hypergraph.src.utils.model_args import ModelArgs
from collaboration_hypergraph.src.gnn_models.graph_model import GraphModel
from collaboration_hypergraph.src.gnn_models.hypergraphconv_model import \
    HypergraphModel
from collaboration_hypergraph.src.training.neptune_util import NeptuneRun


# Load dataset
dataset = CAHepThLinkDataset()


@click.command()
@click.option(
    '--num_layers', default=2, help='Number of GNN layers.', type=int
)
@click.option(
    '--hidden_dim', default=10, help='Size of hidden dimension.', type=int
)
@click.option('--epochs', default=100, help='Number of epochs.', type=int)
@click.option('--lr', default=0.001, help='Learning rate.', type=float)
@click.option(
    '--model_name',
    default='HypergraphConv',
    help='Model name.',
    type=str
)
@click.option(
    '--aggr',
    default='add',
    help='Type of aggregation. ("add", "mean" or "max")',
    type=str
)
@click.option('--dropout', default=0.0, help='Dropout rate.', type=float)
@click.option('--opt', default='adam', help='Optimiser.', type=str)
@click.option('--weight_decay', default=0.0, help='Weight decay.', type=float)
@click.option(
    '--preprocessing_lin_layers',
    default=False,
    help='If True appends preprocessing linear layers.',
    type=bool
)
@click.option(
    '--final_lin_layers',
    default=True,
    help='If True appends preprocessing linear layers.',
    type=bool
)
@click.option(
    '--norm_layers',
    default=False,
    help='If True appends normalisation layers.',
    type=bool
)
@click.option(
    '--encoder_model',
    default='hypergraph',
    help='Graph representation model. ("graph" or "hypergraph")',
    type=str
)
def run_model(
    num_layers: int,
    hidden_dim: int,
    epochs: int,
    lr: float,
    model_name: str,
    aggr: str,
    dropout: float,
    opt: str,
    weight_decay: float,
    preprocessing_lin_layers: bool,
    final_lin_layers: bool,
    norm_layers: bool,
    encoder_model: str
):
    # Load model metadata
    args = {
        'num_layers': num_layers,
        'hidden_dim': hidden_dim,
        'epochs': epochs,
        'lr': lr,
        'model_name': model_name,
        'aggr': aggr,
        'dropout': dropout,
        'opt': opt,
        'weight_decay': weight_decay,
        'preprocessing_lin_layers': preprocessing_lin_layers,
        'final_lin_layers': final_lin_layers,
        'norm_layers': norm_layers
    }
    model_args = ModelArgs(**args)
    run_name = model_args.get_run_name()

    summary_writer_dir = osp.join(
        settings.SUMMARY_WRITER_LOGS_PATH,
        f'{run_name}_{datetime.now().strftime("%Y%m%d-%H%M%S")}'
    )
    writer = SummaryWriter(summary_writer_dir)
    checkpoint_dir = model_args.get_checkpoint_dir()

    neptune_run = NeptuneRun('neptune_config.json')
    params = model_args.get_neptune_params()
    neptune_run.add_parameters('parameters', params)
    neptune_run.add_parameters('encoder_model', encoder_model)

    # Load model
    if encoder_model == 'graph':
        encoder = GraphModel(
            input_dim=dataset.data.x.shape[1],
            hidden_dim=model_args.hidden_dim,
            output_dim=None,
            args=model_args
        )
    else:
        encoder = HypergraphModel(
            input_dim=dataset.data.x.shape[1],
            hidden_dim=model_args.hidden_dim,
            output_dim=None,
            args=model_args
        )
    model = pyg_nn.GAE(encoder)

    # Run train loop
    train(
        model=model,
        dataset=dataset,
        args=model_args,
        model_type=encoder_model,
        writer=writer,
        checkpoint_dir=checkpoint_dir,
        neptune_run=neptune_run.run
    )

    neptune_run.stop_run()


if __name__ == '__main__':
    run_model()
