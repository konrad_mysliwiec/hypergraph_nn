import neptune.new as neptune
from typing import Union

from collaboration_hypergraph.src.utils.utils import load_json


class NeptuneRun(object):
    def __init__(self, config_dir: str) -> None:
        super().__init__()
        neptune_config = load_json(config_dir)
        if neptune_config.get('project') and neptune_config.get('api_token'):
            self.run = neptune.init(
                project=neptune_config.get('project'),
                api_token=neptune_config.get('api_token')
            )
        else:
            self.run = None

    def add_parameters(self, param_name: str, params: Union[str, dict]):
        if self.run:
            self.run[param_name] = params

    def stop_run(self):
        if self.run:
            self.run.stop()
