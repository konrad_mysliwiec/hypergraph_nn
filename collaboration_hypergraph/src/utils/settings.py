import os.path as osp


# RAW DATA
RAW_CA_HEPTH_DATA_PATH = osp.join(
    'collaboration_hypergraph',
    'data',
    'ca_hepth',
    'cit-HepTh-abstracts'
)


# PROCESSED DATA
PROCESSED_CA_HEPTH_DATA_PATH = osp.join(
    'collaboration_hypergraph',
    'data',
    'ca_hepth',
    'processed'
)
EDGE_INDEX_CA_HEPTH_PATH = osp.join(
    PROCESSED_CA_HEPTH_DATA_PATH,
    'edge_index.csv',
)
EDGE_TYPES_CA_HEPTH_PATH = osp.join(
    PROCESSED_CA_HEPTH_DATA_PATH,
    'edge_types.csv',
)
EDGE_YEARS_CA_HEPTH_PATH = osp.join(
    PROCESSED_CA_HEPTH_DATA_PATH,
    'years_index.csv',
)


# ENCODERS
ENCODERS_CA_HEPTH_PATH = osp.join(
    'collaboration_hypergraph',
    'data',
    'ca_hepth',
    'encoders'
)
NODES_LABEL_CA_HEPTH_ENCODER_PATH = osp.join(
    ENCODERS_CA_HEPTH_PATH,
    'nodes_label_encoder.pkl',
)


# DATASET DIRECTORY
CA_HEPTH_DATASET_DIR = osp.join(
    'collaboration_hypergraph',
    'data',
    'ca_hepth'
)
CA_HEPTH_RAW_DATA = osp.join(
    CA_HEPTH_DATASET_DIR,
    'raw_data'
)


# LOGS
SUMMARY_WRITER_LOGS_PATH = osp.join(
    'models_logs',
    'log'
)
MODEL_CHECKPOINTS_PATH = 'checkpoints'
