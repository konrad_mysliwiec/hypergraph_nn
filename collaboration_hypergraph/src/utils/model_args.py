import os.path as osp

from collaboration_hypergraph.src.utils import settings


class ModelArgs(object):
    def __init__(self, **kwargs) -> None:
        self.opt = kwargs.get('opt', 'adam')
        self.opt_scheduler = kwargs.get('opt_scheduler', 'none')
        self.opt_restart = kwargs.get('opt_restart')
        self.opt_decay_step = kwargs.get('opt_decay_step')
        self.opt_decay_rate = kwargs.get('opt_decay_step')
        self.lr = kwargs.get('lr', 0.01)
        self.clip = kwargs.get('clip')
        self.weight_decay = kwargs.get('weight_decay', 0.0)
        self.model_name = kwargs.get('model_name', 'HypergraphConv')
        self.batch_size = kwargs.get('batch_size', 32)
        self.num_layers = kwargs.get('num_layers', 2)
        self.hidden_dim = kwargs.get('hidden_dim', 32)
        self.dropout = kwargs.get('dropout', 0.0)
        self.epochs = kwargs.get('epochs', 200)
        self.task = kwargs.get('task', 'link')
        self.aggr = kwargs.get('aggr', 'mean')
        self.preprocessing_lin_layers = kwargs.get(
            'preprocessing_lin_layers',
            True
        )
        self.final_lin_layers = kwargs.get(
            'final_lin_layers',
            True
        )
        self.norm_layers = kwargs.get(
            'norm_layers',
            True
        )
        self.graph_representation = kwargs.get(
            'graph_representation',
            'hypergraph'
        )
        self.run_name = self.get_run_name()

    def get_run_name(self) -> str:
        run_name = f'{self.graph_representation}_' \
            f'{self.model_name}' \
            f'_dropout_{self.dropout}' \
            f'_hid_dim_{self.hidden_dim}_epochs_{self.epochs}' \
            f'_lr_{self.lr}_weight_decay_{self.weight_decay}' \
            f'_opt_{self.opt}_aggr_{self.aggr}' \
            f'_num_lays_{self.num_layers}_' \
            f'_pre_layers_{self.preprocessing_lin_layers}' \
            f'_final_layers_{self.final_lin_layers}' \
            f'_norm_layers_{self.norm_layers}'
        return run_name

    def get_checkpoint_dir(self) -> str:
        return osp.join(settings.MODEL_CHECKPOINTS_PATH, self.run_name)

    def get_neptune_params(self):
        neptune_params = {
            "opt": self.opt,
            "opt_scheduler": self.opt_scheduler,
            "opt_restart": self.opt_restart,
            "opt_decay_step": self.opt_decay_step,
            "opt_decay_rate": self.opt_decay_rate,
            "lr": self.lr,
            "clip": self.clip,
            "weight_decay": self.weight_decay,
            "model_name": self.model_name,
            "batch_size": self.batch_size,
            "num_layers": self.num_layers,
            "hidden_dim": self.hidden_dim,
            "dropout": self.dropout,
            "epochs": self.epochs,
            "aggr": self.aggr,
            "preprocessing_lin_layers": self.preprocessing_lin_layers,
            "final_lin_layers": self.final_lin_layers,
            "norm_layers": self.norm_layers
        }
        return neptune_params


if __name__ == '__main__':
    args = ModelArgs(**{'lr': 10})
    print(args.lr)
