import json
import os
import pickle


def create_directory(directory: str):
    if not os.path.exists(directory):
        os.makedirs(directory)


def save_to_pickle(python_object, file_path: str):
    with open(file_path, 'wb') as f:
        pickle.dump(python_object, f)


def load_json(file_path: str):
    with open(file_path, 'r') as f:
        json_content = json.load(f)
    return json_content
