import torch
import torch.nn.functional as F
import torch.nn as nn
import torch_geometric.nn as pyg_nn
from collaboration_hypergraph.src.utils.model_args import ModelArgs


class GraphModel(torch.nn.Module):
    def __init__(
        self,
        input_dim: int,
        hidden_dim: int,
        output_dim: int,
        args: ModelArgs
    ):
        super(GraphModel, self).__init__()
        self.convs_layers = nn.ModuleList()
        self.convs_layers.append(
            pyg_nn.GCNConv(input_dim, hidden_dim, aggr=args.aggr)
            )
        self.norm_layers = nn.ModuleList()
        self.norm_layers.append(nn.LayerNorm(input_dim))

        if output_dim is None:
            output_dim = hidden_dim

        for _ in range(args.num_layers-1):
            self.convs_layers.append(
                pyg_nn.GCNConv(hidden_dim, hidden_dim)
                )
            self.norm_layers.append(nn.LayerNorm(hidden_dim))

        self.preprocessing_lin_layers = nn.Sequential(
            nn.Linear(input_dim, input_dim),
            nn.Dropout(args.dropout),
            nn.Linear(input_dim, input_dim)
        )

        self.final_lin_layers = nn.Sequential(
            nn.Linear(hidden_dim, hidden_dim),
            nn.Dropout(args.dropout),
            nn.Linear(hidden_dim, output_dim)
        )
        self.task = args.task
        self.dropout = args.dropout
        self.num_layers = args.num_layers
        self.should_norm_layers = args.norm_layers
        self.shoould_preprocess_lin_layers = args.preprocessing_lin_layers
        self.should_final_lin_layers = args.final_lin_layers

    def forward(self, data, edge_index=None):
        x = data.x
        if self.shoould_preprocess_lin_layers:
            x = self.preprocessing_lin_layers(x)

        if edge_index is None:
            edge_index = data.edge_index

        for layer in range(self.num_layers):
            if self.should_norm_layers:
                x = self.norm_layers[layer](x)
            x = self.convs_layers[layer](x, edge_index)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)

        if self.should_final_lin_layers:
            x = self.final_lin_layers(x)
        out = x

        return out
