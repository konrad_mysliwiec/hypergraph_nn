# from torch_geometric.data.download import download_url
import tarfile
import os.path as osp
import requests
import glob
from itertools import product
import os
from sklearn.preprocessing import LabelEncoder
import numpy as np
import pandas as pd
from typing import Tuple

from collaboration_hypergraph.src.utils.utils import save_to_pickle
from collaboration_hypergraph.src.utils import settings


URL = 'https://snap.stanford.edu/data/cit-HepTh-abstracts.tar.gz'
FILE_NAME = 'cit-HepTh-abstracts'


def download_ca_hepth():
    gzip_file_name = osp.join(
        settings.CA_HEPTH_RAW_DATA,
        f'{FILE_NAME}.tar.gz'
    )
    response = requests.get(URL, allow_redirects=True)
    with open(gzip_file_name, 'wb') as f:
        f.write(response.content)

    tar = tarfile.open(gzip_file_name, "r:gz")
    tar.extractall(osp.join(settings.CA_HEPTH_RAW_DATA, FILE_NAME))
    tar.close()


def get_abs_file_paths() -> list:
    return glob.glob(f"{settings.CA_HEPTH_RAW_DATA}/*/*/*.abs")


def get_year_from_file_path(file_path: str) -> str:
    return os.path.dirname(file_path)[-4:]


def get_publication_dict_from_file(file_path) -> dict:
    year = get_year_from_file_path(file_path)
    authors_list = []
    title = ''
    publication_id = os.path.basename(file_path)
    with open(file_path, 'r') as f:
        for line in f:
            if 'Title:' in line:
                title = line.replace('Title: ', '').replace('\n', '')
            elif 'Authors:' in line:
                new_line = line.replace('Authors: ', '')\
                    .replace(' and ', ', ')\
                    .replace('\n', '')
                authors_list.extend(new_line.split(', '))
            elif 'Author:' in line:
                new_line = line.replace('Author: ', '')\
                    .replace(' and ', ', ')\
                    .replace('\n', '')
                authors_list.extend(new_line.split(', '))
    publication_dict = {
        'authors': authors_list,
        'title': title,
        'year': int(year),
        'publication_id': publication_id
    }
    return publication_dict


def parse_files(file_paths: list):
    publications_dicts = [
        get_publication_dict_from_file(file_path)
        for file_path in file_paths
    ]
    return pd.DataFrame.from_dict(publications_dicts)


def get_edge_index_from_df(df: pd.DataFrame, max_year: int = None) -> list:
    if max_year:
        df = df[[df['year'] < max_year]]
    edge_index = [
        product(pub_authors, pub_authors)
        for pub_authors in df['authors']
    ]
    edge_index = [
        [list(author_tuple), [i]]
        for i, pub_authors in enumerate(edge_index)
        for author_tuple in pub_authors
    ]
    years_index = [
        [row[2]] * len(list(product(row[0], row[0])))
        for row in df.to_numpy()
    ]
    return edge_index, years_index


def encode_edge_index(edge_index: list) -> np.array:
    le = LabelEncoder()
    edge_index = np.array([[edge[0], edge[1]] for edge in edge_index])
    edge_index = edge_index.reshape(-1)
    le.fit(np.array(edge_index))
    edge_index = le.transform(edge_index)
    edge_index = edge_index.reshape(-1, 2)
    save_to_pickle(le, settings.NODES_LABEL_CA_HEPTH_ENCODER_PATH)
    return edge_index


def get_edge_index_and_types(max_year: int = None) -> Tuple[list, list]:
    file_paths = get_abs_file_paths()
    df = parse_files(file_paths)
    edge_index, years_index = get_edge_index_from_df(df, max_year)
    edge_index, edge_types = np.array(edge_index).T

    edge_index = encode_edge_index(edge_index)
    edge_types = np.array(
        [item for list in edge_types for item in list]
    )[:, None]
    years_index = np.array(
        [item for list in years_index for item in list]
    )[:, None]
    return edge_index, edge_types, years_index


def save_edges_index_to_csvs():
    edge_index, edge_types, years_index = get_edge_index_and_types()
    pd.DataFrame(edge_index).to_csv(
        settings.EDGE_INDEX_CA_HEPTH_PATH,
        index=False,
        header=False
    )
    pd.DataFrame(edge_types).to_csv(
        settings.EDGE_TYPES_CA_HEPTH_PATH,
        index=False,
        header=False
    )
    pd.DataFrame(years_index).to_csv(
        settings.EDGE_YEARS_CA_HEPTH_PATH,
        index=False,
        header=False
    )


if __name__ == '__main__':
    save_edges_index_to_csvs()
