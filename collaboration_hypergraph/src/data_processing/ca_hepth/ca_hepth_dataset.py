import glob
import os.path as osp
from torch_geometric.data import InMemoryDataset
import torch
from typing import Callable, List, Optional, Tuple, Union

from collaboration_hypergraph.src.utils import settings
from collaboration_hypergraph.src.utils.utils import create_directory
from collaboration_hypergraph.src.data_processing.ca_hepth.ca_hepth_utils \
    import download_ca_hepth, save_edges_index_to_csvs
from collaboration_hypergraph.src.data_processing.ca_hepth.ca_hepth_data \
    import collaboration_hypergraph_data


class CAHepThLinkDataset(InMemoryDataset):
    def __init__(
        self,
        transform: Optional[Callable] = None,
        pre_transform: Optional[Callable] = None,
        pre_filter: Optional[Callable] = None,
        train_max_year: int = 1999,
        val_max_year: int = 2001,
        test_max_year: int = 2003,
        features: Optional[List] = None
    ):
        self.root = settings.CA_HEPTH_DATASET_DIR
        self.features = features
        self.train_max_year = train_max_year
        self.val_max_year = val_max_year
        self.test_max_year = test_max_year
        super(CAHepThLinkDataset, self).__init__(
            root=self.root,
            transform=transform,
            pre_transform=pre_transform,
            pre_filter=pre_filter,
        )
        self.data, self.slices = torch.load(self.processed_paths[0])
        create_directory(self.root)

    @property
    def raw_file_names(self) -> Union[str, List[str], Tuple]:
        return []

    @property
    def processed_dir(self) -> str:
        return osp.join(self.root, 'processed')

    @property
    def processed_file_names(self) -> Union[str, List[str], Tuple]:
        return ['data_0.pt']

    def download(self):
        processed_files = glob.glob(f'{self.processed_dir}/*')
        processed_files = [
            file_path.split('/')[-1] for file_path in processed_files
        ]
        existing_processed_file_names = [
            processed_file_name in processed_files
            for processed_file_name in self.processed_file_names
        ]
        if sum(existing_processed_file_names) < len(self.processed_file_names):
            download_ca_hepth()

    def process(self):
        save_edges_index_to_csvs()

        data_list = [
            collaboration_hypergraph_data(
                egde_index_csv_file_path=settings.EDGE_INDEX_CA_HEPTH_PATH,
                edge_ids_csv_file_path=settings.EDGE_TYPES_CA_HEPTH_PATH,
                years_csv_file_path=settings.EDGE_YEARS_CA_HEPTH_PATH,
                features=None,
                train_max_year=self.train_max_year,
                val_max_year=self.val_max_year,
                test_max_year=self.test_max_year
            )
        ]
        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])


if __name__ == '__main__':
    dataset = CAHepThLinkDataset()
    print(dataset.data)
