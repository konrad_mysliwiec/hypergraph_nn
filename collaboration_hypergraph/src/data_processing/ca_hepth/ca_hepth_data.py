import os.path as osp
import pandas as pd
from torch_geometric.data import Data
from torch_geometric.utils import negative_sampling
import torch_geometric as pyg
import torch

from collaboration_hypergraph.src.utils import settings


def collaboration_hypergraph_data(
    egde_index_csv_file_path: str,
    edge_ids_csv_file_path: str,
    years_csv_file_path: str,
    features: list = None,
    train_max_year: int = 1999,
    val_max_year: int = 2001,
    test_max_year: int = 2003
) -> pyg.data.Dataset:
    """Returns Data object for Hypergraph Collaboration Network.

    Args:
        egde_index_csv_file_path (str): File path to a csv file
            with edge index.
        edge_ids_csv_file_path (str): Filepath to a file with edges ids.
        edge_ids_csv_file_path (str): Filepath to a file with years
            of publications.
        features (str, optional): Nodes features. Defaults to 'ones'.
        train_max_year (int, optional): Maximum publication year
            used for training dataset. Defaults to 1999.
        val_maxy_year (int, optional): Maximum publication year
            used for validation dataset. Defaults to 2001.
        test_max_year (int, optional): Maximum publication year
            used for validation dataset. Defaults to 2003.

    Returns:
        pyg.data.Dataset: Dataset for Hypergraph Collaboration Network.
    """
    edge_index_df = pd.read_csv(egde_index_csv_file_path, header=None)
    edge_ids_df = pd.read_csv(edge_ids_csv_file_path, header=None)
    years_index_df = pd.read_csv(years_csv_file_path, header=None)
    edge_index = torch.tensor(
        [
            edge_index_df[0],
            edge_index_df[1]
        ],
        dtype=torch.long
    )
    edge_ids = torch.tensor(edge_ids_df[0], dtype=torch.long)
    years_index = torch.tensor(years_index_df[0], dtype=torch.long)
    data = train_test_split(
        edge_index=edge_index,
        edge_ids=edge_ids,
        years_index=years_index,
        train_max_year=train_max_year,
        val_max_year=val_max_year,
        test_max_year=test_max_year
    )
    num_nodes = edge_index.max()+1
    if features is None:
        x = torch.ones(num_nodes, 10, dtype=torch.float32)
    else:
        x = torch.tensor(features, dtype=torch.float32)

    data.x = x
    data.num_nodes = num_nodes

    return data


def train_test_split(
    edge_index: torch.Tensor,
    edge_ids: torch.Tensor,
    years_index: torch.Tensor,
    train_max_year: int,
    val_max_year: int,
    test_max_year: int
):

    train_mask = years_index <= train_max_year
    val_mask = (years_index > train_max_year) * (years_index <= val_max_year)
    test_mask = (years_index > val_max_year) * (years_index <= test_max_year)

    pos_edge_index_train = edge_index[:, train_mask]
    pos_edge_index_val = edge_index[:, val_mask]
    pos_edge_index_test = edge_index[:, test_mask]

    edge_ids_train = edge_ids[train_mask]
    edge_ids_val = edge_ids[val_mask]
    edge_ids_test = edge_ids[test_mask]

    hyperedge_index_train = get_hyperedge_index(
        pos_edge_index_train,
        edge_ids_train
    )
    hyperedge_index_val = get_hyperedge_index(
        pos_edge_index_train,
        edge_ids_train
    )
    hyperedge_index_test = get_hyperedge_index(
        pos_edge_index_train,
        edge_ids_train
    )

    years_index_train = years_index[train_mask]
    years_index_val = years_index[val_mask]
    years_index_test = years_index[test_mask]

    neg_edge_index_train = negative_sampling(
        pos_edge_index_train,
        num_nodes=pos_edge_index_train.unique().shape[0],
        num_neg_samples=pos_edge_index_train.shape[1]
    )
    neg_edge_index_val = negative_sampling(
        pos_edge_index_val,
        num_nodes=pos_edge_index_val.unique().shape[0],
        num_neg_samples=pos_edge_index_val.shape[1]
    )
    neg_edge_index_test = negative_sampling(
        pos_edge_index_test,
        num_nodes=pos_edge_index_test.unique().shape[0],
        num_neg_samples=pos_edge_index_test.shape[1]
    )

    data = Data(
        train_pos_edge_index=pos_edge_index_train,
        val_pos_edge_index=pos_edge_index_val,
        test_pos_edge_index=pos_edge_index_test,
        train_neg_edge_index=neg_edge_index_train,
        val_neg_edge_index=neg_edge_index_val,
        test_neg_edge_index=neg_edge_index_test,
        edge_ids_train=edge_ids_train,
        edge_ids_val=edge_ids_val,
        edge_ids_test=edge_ids_test,
        years_index_train=years_index_train,
        years_index_val=years_index_val,
        years_index_test=years_index_test,
        hyperedge_index_train=hyperedge_index_train,
        hyperedge_index_val=hyperedge_index_val,
        hyperedge_index_test=hyperedge_index_test
    )

    return data


def get_incidence_matrix_from_edge_index(
    edge_index: torch.Tensor,
    edge_ids: torch.Tensor,
    num_nodes: int,
    num_edges: int
):
    edge_index = edge_index.T
    A = torch.zeros(num_nodes, num_edges)
    edge_index_0 = torch.cat(
        [
            edge_index[:, 0].unsqueeze(dim=1),
            edge_ids.unsqueeze(dim=1)
        ],
        dim=1
    )
    A[tuple(edge_index_0.T)] = 1
    edge_index_0 = torch.cat(
        [
            edge_index[:, 1].unsqueeze(dim=1),
            edge_ids.unsqueeze(dim=1)
        ],
        dim=1
    )
    A[tuple(edge_index_0.T)] = 1
    return A


def get_hyperedge_index(
    edge_index: torch.Tensor,
    edge_ids: torch.Tensor
):
    edge_index = edge_index.T
    edge_index_0 = torch.cat(
        [
            edge_index[:, 0].unsqueeze(dim=1),
            edge_ids.unsqueeze(dim=1)
        ],
        dim=1
    )
    edge_index_1 = torch.cat(
        [
            edge_index[:, 1].unsqueeze(dim=1),
            edge_ids.unsqueeze(dim=1)
        ],
        dim=1
    )
    hyperedge_index = torch.cat(
        [edge_index_0, edge_index_1]
    )
    return hyperedge_index.T


if __name__ == '__main__':
    data = collaboration_hypergraph_data(
        egde_index_csv_file_path=settings.EDGE_INDEX_CA_HEPTH_PATH,
        edge_ids_csv_file_path=settings.EDGE_TYPES_CA_HEPTH_PATH,
        years_csv_file_path=settings.EDGE_YEARS_CA_HEPTH_PATH
    )
    print(data)
